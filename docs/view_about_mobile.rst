View About
==========

1. Tap ‘About’.

.. image:: images/view_about_mobile_1.png
   :align: center

2. List of Raqib social media displayed.

.. image:: images/view_about_mobile_2.png
   :align: center