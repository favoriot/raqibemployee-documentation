View Alert
==========

1. Tap ‘Alert’.

.. image:: images/view_alert_mobile_1.png
   :align: center

2.  Tap on the list of alert that you want to view.

.. image:: images/view_alert_mobile_2.png
   :align: center

3.  View alert with map and time detail.

.. image:: images/view_alert_mobile_3.png
   :align: center