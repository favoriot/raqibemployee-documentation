View Employee's Information
==========================

1. Tap ‘Profile’.

.. image:: images/view_employees_information_mobile_1.png
   :align: center

2. Employee’s profile information displayed.

.. image:: images/view_employees_information_mobile_2.png
   :align: center