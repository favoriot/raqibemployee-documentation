Application Installation
========================

There are two ways to install the application onto your mobile 
devices.

1. Download Raqib App from `App Store`_ or `Play Store`_.

.. _App Store: http://appstore.com/
.. _Play Store: https://play.google.com/store

OR

2. Scan QR code inside Raqib wearable

   a. Swipe right or left until you reach Setting page

   .. image:: images/application_installation_wearable_1.png
      :align: center
   
   b. Tap Setting, then navigate to Watch bind.

   .. image:: images/application_installation_wearable_2.png
      :align: center

   c. Scan QR code from your mobile phone to go to `Raqib website
      <http://raqib.co/>`_.

   .. image:: images/application_installation_wearable_3.png
      :align: center

   d. Tap download link according to your mobile operating 
      system.
   
   .. image:: images/application_installation_wearable_4.png
      :align: center

