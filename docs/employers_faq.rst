Employers FAQ
==================

Can I monitor my employees near real – time location?
-----------------------------------------------------

Yes. You can monitor their location through the Raqib Mobile 
App and Raqib Dashboard.

Can I monitor my employees’ health status?
------------------------------------------

Yes. You can monitor their location through the Raqib Mobile 
App and Raqib Dashboard.

Is there any employer number that I can contact via the app?
------------------------------------------------------------

Yes.

How often are the information being updated?
--------------------------------------------

Every 5 minutes.

Can I send text message to my employees through Raqib?
------------------------------------------------------

No.

Can I call my employees through Raqib?
--------------------------------------

Yes. There will be call charges by the telco.