View ECG
========

1. Tap ‘ECG’.

.. image:: images/view_ecg_mobile_1.png
   :align: center

2. List of ECG taken by employee. Tap ‘Expand’ icon button to 
   view ECG information.

.. image:: images/view_ecg_mobile_2.png
   :align: center

3. ECG result with indicator.

.. image:: images/view_ecg_mobile_3.png
   :align: center