Call Employer
=============

1. Tap ‘Profile’.

.. image:: images/call_employer_mobile_1.png
   :align: center

2.  Click employer number in the Emergency Contact.

.. image:: images/call_employer_mobile_2.png
   :align: center

3.  Make call to the employer.

.. image:: images/call_employer_mobile_3.png
   :align: center