Employees FAQ
============

What if I misplace Raqib? Can it be found?
------------------------------------------

You can refer the last GPS location.

I think I’m lost. Can someone be able to detect my whereabouts? Will I be rescued?
----------------------------------------------------------------------------------

Yes, you need to press the SOS button. And employer will call you for further assistance.

Can I send messages to employer?
--------------------------------------------------------

No.

How can I find my employer for assistance?
------------------------------------------

You can press the SOS button and an alert will be sent to the 
employer.

Will Raqib replace my phone?
----------------------------

Partly, Raqib can only receive call at the moment.

Can my employer know my real-time location?
-------------------------------------------

Yes. They can know through the Raqib Mobile App.

I accidentally switched my Raqib at workplace. How can I switch it back?
----------------------------------------------------------------------------

You can call your Raqib Mobile Number to identify your Raqib.

Is there a signal coverage in Malaysia?
---------------------------------------

Yes.

Does anyone monitor my health status?
-------------------------------------

Yes. Your employer.

How frequent do I need to charge my Raqib?
------------------------------------------

You need to charge Raqib Flexi-1 in every two days.

Can I put my Raqib on a silent mode?
------------------------------------

No. But you can immediately reject the call.